const arr = [1, 2, 2, 3, 4, 4, 5];

function removeDuplicate(data) {
    return [...new Set(data)];
}

console.log(removeDuplicate(arr));